package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

/*interface segregation principle being used- 
Many client-specific interfaces are better than one general-purpose interface. 
which is why worker and super worker class are seperated
*/

class SuperWorker implements Workable, Feedable{
    public void work() {
		System.out.println("Working a lot ...");
	}

	public void eat() {
		System.out.println("Drinking 5-hour energy ...");
	}
}