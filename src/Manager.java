// This class does not use Interface Segration.
// This class is open to extension but closed for modification of its source code.
// This class uses Single Responisbility principle with the setWoker and manage methods.

package threesolid;

class Manager{
    Workable worker;
    
    public void setWorker(Workable w){
        worker = w;
    }

    public void manage() {
        worker.work();
    }
}