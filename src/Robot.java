
package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

// Interface Segregation being used in this class to make the class flexible
// Single responsibility principle with the work and eat interfaces

class Robot implements Workable{
    public void work() {
        System.out.println("Beep boop working ...");
    }

}