
package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

// This class uses Interface Segregation to make the class more flexable.
// I also see the single responsibility principle with the work and eat interfaces.

class Worker implements Workable, Feedable{
    public void work() {
        System.out.println("Working ...");
    }

    public void eat() {
        System.out.println("Eating ...");
    }
}