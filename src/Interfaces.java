/* Single Responsibility Principle - Does not apply as the interface did not have more than
 * one responsibility over program functionality.
 *
 * Open-Close Principle - Does not apply as the interface was already open to extension, and
 * there was no definition to be modified.
 *
 * Interface Segregation Principle - The functions work() and eat() have been seperated into
 * their own interfaces such that a class can implement them based on need, rather than
 * requiring both for every implementation.
 *
 */

package threesolid;

interface Workable {
	public void work();
}

interface Feedable {
	public void eat();
}