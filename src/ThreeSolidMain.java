package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

// Adding a comment.


public class ThreeSolidMain
{   

   public static Manager tsManager = new Manager();

   // The entry main() method
   public static void main(String[] args) 
   {
 
      try 
      {
         System.out.format("Starting ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      System.out.println("\nTesting Manager and Worker ...\n");

      Worker tom = new Worker();
      tsManager.setWorker(tom);
      tsManager.manage();
      tom.eat();
      tom.eat();
      tsManager.manage();
      tsManager.manage();

      System.out.println("\n\nTesting SuperWorker ...\n");

      SuperWorker dick = new SuperWorker();
      tsManager.setWorker(dick);
      tsManager.manage();
      tsManager.manage();
      dick.eat();
      tsManager.manage();
      tsManager.manage();
      tsManager.manage();

      System.out.println("\n\nTesting Robot ...\n");

      Robot harry = new Robot();
      tsManager.setWorker(harry);
      tsManager.manage();
      tsManager.manage();
      tsManager.manage();
      tsManager.manage();
      tsManager.manage();
      tsManager.manage();
      tsManager.manage();

      System.out.println();

      try 
      {
         System.out.format("Stopping ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      System.exit(0);

   }
 }
